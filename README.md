# ShittyPixiParticle

Shitty particle class to Pixijs.
It doesn't contain everything a particle library should contain. However, simple particle effects can be done.

#Usage Example

`
var po = {
	count : 8,
	spin  : false,
	angleBegin : 0,
	angleEnd :  2 * Math.PI
}
var particle = new Particle(pic0Texture, app.stage, po);
particle.create(2 * step, y * 0.15, step, step);
`

Create 8 particles, no random spin and particles will have an angle of motion between 0 and 360 degrees.

`var po = {
	count : 16,
	spin  : true,
	angleBegin : 0,
	angleEnd :  Math.PI
}
var particle = new Particle(pic0Texture, app.stage, po);
particle.create(2 * step, y * 0.45, step, step);`


Create 16 particles, particles spinned randomly and particles will have an angle of motion between 0 and 180 degrees.

`var po = {
	count : 500,
	spin  : false,
	angleBegin : Math.PI / 2,
	angleEnd :  Math.PI / 2
}
var particle = new Particle(pic0Texture, app.stage, po);
particle.create(x * 0.6 + step, y * 0.75, step, step);`


Create 500 particles, no random spin and particles move linearly.(so angleBegin and angleEnd parameter is equal)

`new Particle(pic0Texture, app.stage, po);`

First parameter is texture to sprite.
Second parameter is container that sprites are added on it.
Third parameter is object that It carries the values of variables related to the behavior of particles. If omitted, the snippets behave as default.
