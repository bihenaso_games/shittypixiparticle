function Particle(texture, container, properties = {}){
	//Destructing parameters mechanism
	var {	
		ticker = 60,
		duration = 1000,
		distance = 60,
		count = 1,
		angleBegin = Math.PI,
		angleEnd = Math.PI,
		alphaBegin = 1,
		alphaEnd = 0.3,
		rotationBegin = 0,
		rotationEnd = 2 * Math.PI,
		scaleFactor = 0.5,
		finishDeath = true,
		finishContainer = null,
		spin = true	
} = properties;

	this.texture = texture;
	this.container = container;
	this.ticker = ticker;
	this.duration = duration;
	this.distance = distance;
	this.count = count;
	this.angleBegin = angleBegin;
	this.angleEnd = angleEnd;
	this.alphaBegin = alphaBegin;
	this.alphaEnd = alphaEnd;
	this.rotationBegin = rotationBegin;
	this.rotationEnd = rotationEnd;
	this.scaleFactor = scaleFactor;
	this.finishDeath = finishDeath;
	this.finishContainer = finishContainer;
	this.spin = spin;
	this.create = function(x, y, width, height){
		var dur = 1000 / (this.angleBegin / this.angleEnd);
		animUtil(dur, this.count, function(){this.doItUtil(this.createSprite(x, y, width, height))}.bind(this));
	}
	this.createSprite = function(x, y, width, height){
			var sprite = new PIXI.Sprite(this.texture);
			sprite.x = x;
			sprite.y = y;
			sprite.width = width;
			sprite.height = height;
			sprite.anchor.set(0.5);
			sprite.alpha = this.alphaBegin;
			sprite.rotation = this.rotationBegin;
			sprite.angle = randomAngle(this.angleBegin, this.angleEnd);
			sprite.dstep = this.distance / this.ticker;
			sprite.astep = (this.alphaBegin - this.alphaEnd) / this.ticker;
			sprite.rstep = (this.rotationBegin - this.rotationEnd) / this.ticker;
			sprite.wstep = (sprite.width * this.scaleFactor) / this.ticker;
			sprite.hstep = (sprite.height * this.scaleFactor) / this.ticker;
			this.container.addChild(sprite);
			return sprite;	
	}
	this.doItUtil = function(sprite){
		animUtil(this.duration, this.ticker, function(){this.doIt(sprite)}.bind(this), function(){this.rmIt(sprite)}.bind(this));
	}
	this.doIt = function(sprite){
		var factor = this.spin > 0 ? Math.random() : 1;
		sprite.x += sprite.dstep * Math.sin(factor * sprite.angle);
		sprite.y -= sprite.dstep * Math.cos(factor * sprite.angle);
		sprite.alpha -= sprite.astep;
		sprite.rotation -= sprite.rstep;
		sprite.width -= sprite.wstep;
		sprite.height -= sprite.hstep; 
	}
	this.rmIt = function(sprite){
		if(finishDeath){
			animUtil(this.duration / 4, 4, function(){sprite.alpha -= sprite.alpha / 4;}.bind(this), 
				function(){this.container.removeChild(sprite);}.bind(this));
		}else{
			this.finishContainer.addChild(sprite);
			this.container.removeChild(sprite);
		}
	}		
	
}

Particle.prototype = Object.create(Object.prototype);
Object.defineProperty(Particle.prototype, 'constructor', {
  value: Particle,
  enumarable: false,
  writable: true
});

function animUtil(duration, maxCount, animfunc, callback = null){
  anim(duration, maxCount, maxCount, animfunc, callback);
}
function anim(duration, maxCount, count, animfunc, callback){
  if(count < 1){
    if(callback != null) return callback();
    else return;
  }else{
    animfunc();
    setTimeout(function(){
      anim(duration, maxCount, --count, animfunc, callback);
    }, duration / maxCount);
  }
}
function randomAngle(begin, end){
	return (Math.random() * (end - begin) + begin);
}
