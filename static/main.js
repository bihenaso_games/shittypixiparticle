var path = "./img/";
var x = window.innerWidth;
var y = window.innerHeight;
var step = x > y ? y * 0.05 : x * 0.05;

var app = new PIXI.Application({
    width: x,
    height: y,
    antialias: true,
    transparent: false,
    resolution: window.devicePixelRatio
  }
);
app.renderer.backgroundColor = 0x0A0A08;
app.renderer.transparent = true;
app.renderer.view.style.display = "block";

document.body.appendChild(app.view);

var pic0Texture = new PIXI.Texture.fromImage(path + "pic0.png");
var text = new PIXI.Text("360deg", {"fontsize" : step + "px", "fill" : "#ffffff"});
text.x = step;
text.y = y * 0.3;
text.buttonMode = true;
text.interactive = true;
text.on("pointerdown", p360);
app.stage.addChild(text);

var text1 = new PIXI.Text("180deg", {"fontsize" : step + "px", "fill" : "#ffffff"});
text1.x = x * 0.3;
text1.y = y * 0.3;
text1.buttonMode = true;
text1.interactive = true;
text1.on("pointerdown", p180);
app.stage.addChild(text1);

var text2 = new PIXI.Text("linear", {"fontsize" : step + "px", "fill" : "#ffffff"});
text2.x = x * 0.6;
text2.y = y * 0.3;
text2.buttonMode = true;
text2.interactive = true;
text2.on("pointerdown", linear);
app.stage.addChild(text2);

var text3 = new PIXI.Text("360deg with spin", {"fontsize" : step + "px", "fill" : "#ffffff"});
text3.x = step;
text3.y = y * 0.6;
text3.buttonMode = true;
text3.interactive = true;
text3.on("pointerdown", p360spin);
app.stage.addChild(text3);

var text4 = new PIXI.Text("180deg with spin", {"fontsize" : step + "px", "fill" : "#ffffff"});
text4.x = x * 0.3;
text4.y = y * 0.6;
text4.buttonMode = true;
text4.interactive = true;
text4.on("pointerdown", p180spin);
app.stage.addChild(text4);

var text5 = new PIXI.Text("linear with spin", {"fontsize" : step + "px", "fill" : "#ffffff"});
text5.x = x * 0.6;
text5.y = y * 0.6;
text5.buttonMode = true;
text5.interactive = true;
text5.on("pointerdown", linearspin);
app.stage.addChild(text5);

var text6 = new PIXI.Text("360deg 2000 particles", {"fontsize" : step + "px", "fill" : "#ffffff"});
text6.x = step;
text6.y = y * 0.9;
text6.buttonMode = true;
text6.interactive = true;
text6.on("pointerdown", p360p);
app.stage.addChild(text6);

var text7 = new PIXI.Text("180deg 2000 particles", {"fontsize" : step + "px", "fill" : "#ffffff"});
text7.x = x * 0.3;
text7.y = y * 0.9;
text7.buttonMode = true;
text7.interactive = true;
text7.on("pointerdown", p180p);
app.stage.addChild(text7);

var text8 = new PIXI.Text("linear 2000 particles", {"fontsize" : step + "px", "fill" : "#ffffff"});
text8.x = x * 0.6;
text8.y = y * 0.9;
text8.buttonMode = true;
text8.interactive = true;
text8.on("pointerdown", linearp);
app.stage.addChild(text8);

function p360(){
	var po = {
		count : 8,
		spin  : false,
		angleBegin : 0,
		angleEnd :  2 * Math.PI
	}
	var particle = new Particle(pic0Texture, app.stage, po);
	particle.create(2 * step, y * 0.15, step, step);
}

function p180(){
	var po = {
		count : 8,
		spin  : false,
		angleBegin : 0,
		angleEnd :  Math.PI
	}
	var particle = new Particle(pic0Texture, app.stage, po);
	particle.create(x * 0.3 + step, y * 0.15, step, step);
}

function linear(){
	var po = {
		count : 8,
		spin  : false,
		angleBegin : Math.PI / 2,
		angleEnd :  Math.PI / 2
	}
	var particle = new Particle(pic0Texture, app.stage, po);
	particle.create(x * 0.6 + step, y * 0.15, step, step);
}

function p360spin(){
	var po = {
		count : 8,
		spin  : true,
		angleBegin : 0,
		angleEnd :  2 * Math.PI
	}
	var particle = new Particle(pic0Texture, app.stage, po);
	particle.create(2 * step, y * 0.45, step, step);
}

function p180spin(){
	var po = {
		count : 8,
		spin  : true,
		angleBegin : 0,
		angleEnd :  Math.PI
	}
	var particle = new Particle(pic0Texture, app.stage, po);
	particle.create(x * 0.3 + step, y * 0.45, step, step);
}

function linearspin(){
	var po = {
		count : 8,
		spin  : true,
		angleBegin : Math.PI / 2,
		angleEnd :  Math.PI / 2
	}
	var particle = new Particle(pic0Texture, app.stage, po);
	particle.create(x * 0.6 + step, y * 0.45, step, step);
}

function p360p(){
	var po = {
		count : 2000,
		spin  : false,
		angleBegin : 0,
		angleEnd :  2 * Math.PI
	}
	var particle = new Particle(pic0Texture, app.stage, po);
	particle.create(2 * step, y * 0.75, step, step);
}

function p180p(){
	var po = {
		count : 2000,
		spin  : false,
		angleBegin : 0,
		angleEnd :  Math.PI
	}
	var particle = new Particle(pic0Texture, app.stage, po);
	particle.create(x * 0.3 + step, y * 0.75, step, step);
}

function linearp(){
	var po = {
		count : 2000,
		spin  : false,
		angleBegin : Math.PI / 2,
		angleEnd :  Math.PI / 2
	}
	var particle = new Particle(pic0Texture, app.stage, po);
	particle.create(x * 0.6 + step, y * 0.75, step, step);
}
